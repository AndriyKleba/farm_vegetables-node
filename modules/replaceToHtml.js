module.exports =
    (temp, product) => {
        let outputElement = temp.replace(/{%ID%}/g, product.id)
        outputElement = outputElement.replace(/{%PRODUCT_NAME%}/g, product.productName),
            outputElement = outputElement.replace(/{%IMAGE%}/g, product.image),
            outputElement = outputElement.replace(/{%FROM%}/g, product.from),
            outputElement = outputElement.replace(/{%NUTRIENTS%}/g, product.nutrients),
            outputElement = outputElement.replace(/{%QUANTITY%}/g, product.quantity),
            outputElement = outputElement.replace(/{%PRICE%}/g, product.price),
            outputElement = outputElement.replace(/{%DESCRIPTION%}/g, product.description);
        if (!product.organic) {
            outputElement = outputElement.replace(/{%NOT_ORGANIC%}/g, ' not-organic')
        }
        return outputElement;
    }

