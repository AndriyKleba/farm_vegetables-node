const fs = require('fs');
const http = require('http');
const url = require('url');
const replaceToHtml = require('./templates/modules/replaceToHtml')

const overview = fs.readFileSync(`${__dirname}/templates/overview.html`, 'utf-8')
const product = fs.readFileSync(`${__dirname}/templates/product.html`, 'utf-8')
const card = fs.readFileSync(`${__dirname}/templates/card.html`, 'utf-8')

const db = fs.readFileSync(`${__dirname}/dev-data/data.json`, 'utf-8')
const dbObject = JSON.parse(db)



const server = http.createServer((req, res) => {
    const {query, pathname} = url.parse(req.url, true)

    if (pathname === '/' || pathname === '/overview') {
        res.writeHead(200, {'Content-type': 'text/html'})
        const cardsHtml = dbObject.map(el => replaceToHtml(card, el)).join('')
        const output = overview.replace(/{%PRODUCT_CARDS%}/, cardsHtml)
        res.end(output)
    } else if (pathname === '/product') {
        res.writeHead(200, {'Content-type': 'text/html'})
        const productId = dbObject[query.id]
        const output = replaceToHtml(product, productId)
        res.end(output)
    } else {
        res.end('git')
    }
})

server.listen(4000, '127.0.0.1', () => {
    console.log('Listener to request from port 4000')
})